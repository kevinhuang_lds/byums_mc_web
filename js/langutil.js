
$(document).ready(function() {
    changeLang('en');   //default language
});

var anotherLang = function() {
    var lang = $('#selLang').val();
    // alert(lang);
    changeLang(lang);
};

/* ========   處理多國語系用的  ===========  */
var changeLang = function(langCode) {
    var key = 'i18n-text';
    var keyPlaceHolder = 'i18n-placeholder';
    $(document).find('*').each(function(index, element) {
        var textKey = $(this).attr(key);
        if (textKey) {
            var translatedText = langResp[langCode][textKey];
            if (translatedText) {
                $(this).text(translatedText);
            }
            else {
                $(this).text(textKey);
            }
        }

        var placeHolderKey = $(this).attr(keyPlaceHolder);
        if (placeHolderKey) {
            var placeHolder = langResp[langCode][placeHolderKey];
            if (placeHolder) {
                $(this).attr("placeholder",placeHolder);
            }
            else {
                $(this).attr("placeholder",placeHolderKey);
            }
        }
    })
}
/* ========   處理多國語系用的  ===========  */